package com.iatskaer.books.servlet;

import com.iatskaer.books.Book;
import com.iatskaer.books.BookDB;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            String title = request.getParameter("title");
            String author = request.getParameter("author");
            Book book = new Book(id, title, author);
            BookDB.update(book);
            response.sendRedirect(request.getContextPath() + "/view");
        } catch (Exception ex) {
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            Book book = BookDB.selectOne(id);
            if (book != null) {
                request.setAttribute("book", book);
                request.getRequestDispatcher("/update.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/error.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            request.getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }
}
